﻿using Reselling.Domain.Models.Data;

namespace Reselling.UIView.Models.Data
{
    public class EventTicketsListView
    {
        public Event Event { get; set; }

        public PagingInfo PagingInfo { get; set; }
    }
}
