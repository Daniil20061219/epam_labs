﻿using System;
using System.IO;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Reselling.UIView.Models.Data;
using System.Text.Encodings.Web;

namespace Resaling.Helpers
{
    public static class PagingHelpers
    {
        public static HtmlString PageLinks(this IHtmlHelper html, PagingInfo pagingInfo, Func<int, string> pageUrl)
        {
            TagBuilder _ul = new TagBuilder("ul");
            _ul.AddCssClass("pagination");
            for (int i = 1; i <= pagingInfo.TotalPages; i++)
            {
                TagBuilder _li = new TagBuilder("li");
                TagBuilder _a = new TagBuilder("a");
                _a.MergeAttribute("href", pageUrl(i));
                _a.InnerHtml.Append(i.ToString());
                _li.InnerHtml.AppendHtml(_a);

                if (i == pagingInfo.CurentPage)
                    _li.AddCssClass("active");
                _ul.InnerHtml.AppendHtml(_li);
            }

            TagBuilder _divCol = new TagBuilder("div");
            _divCol.AddCssClass("col-md-12 text-center");
            _divCol.InnerHtml.AppendHtml(_ul);

            TagBuilder _divRow = new TagBuilder("div");
            _divRow.AddCssClass("row");
            _divRow.InnerHtml.AppendHtml(_divCol);

            TagBuilder _divCon = new TagBuilder("div");
            _divCon.AddCssClass("container");
            _divCon.InnerHtml.AppendHtml(_divRow);

            TagBuilder _divSec = new TagBuilder("div");
            _divSec.AddCssClass("section");
            _divSec.InnerHtml.AppendHtml(_divCon);

            TextWriter writer = new StringWriter();
            _divSec.WriteTo(writer, HtmlEncoder.Default);

            return new HtmlString(writer.ToString());
       }
    }
}
