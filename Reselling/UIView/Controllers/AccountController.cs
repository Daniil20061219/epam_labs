﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Reselling.UIView.Models.Authentication;
using Reselling.Domain.Infrastructure;
using Reselling.Domain.Models.Authentication;

namespace Reselling.UIView.Controllers
{
    public class AccountController : Controller
    {
        IRepository _repo;

        public AccountController(IRepository repo)
        {
            _repo = repo;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
     //   [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = await _repo.GetUserByEmailAndPasswordAsync(model.Email, model.Password);
                if (user != null)
                {
                    await Authenticate(model.Email); 

                     return RedirectToAction("Events", "Main");
                }
                //else
                //    ModelState.AddModelError("", "Uncorrect");
            }
            return View(model);
        }
        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = await _repo.GetUserByEmailAsync(model.Email);
                if (user == null)
                {

                    _repo.AddUser(model.Email, model.Password);
                   
                    await Authenticate(model.Email); 

                    return RedirectToAction("Events", "Main");
                }
                //else
                //    ModelState.AddModelError("", "Uncorrect");
            }
            return View(model);
        }

        private async Task Authenticate(string userName)
        {

            var claims = new List<Claim>
                    {
                        new Claim(ClaimsIdentity.DefaultNameClaimType, userName)
                    };

            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);

            await HttpContext.Authentication.SignInAsync("MyCookies", new ClaimsPrincipal(id));
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.Authentication.SignOutAsync("MyCookies");
            return RedirectToAction("Login", "Account");
        }

    }

}
