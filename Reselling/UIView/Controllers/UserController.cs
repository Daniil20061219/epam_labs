﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Reselling.Domain.Infrastructure;
using Reselling.Domain.Models.Data;
using Reselling.UIView.Models.Data;
using Reselling.Domain.Models.Authentication;

namespace Resaling.Controllers
{
    public class UserController : Controller
    {
        
        IRepository _repo;

        public UserController(IRepository repo)
        {
            _repo = repo;
        }


        public IActionResult MyTickets()
        {

            User _user = _repo.GetUserByEmail(HttpContext.User.Identity.Name);
            if (_user != null)
            {
                UserTicketsViewModel _userTickets = new UserTicketsViewModel
                {
                    User = _user,
                    SellingTikets = _user.Tickets.Where<Ticket>(t => t.TicketStatusId == (int)TicketStatusEnum.SellingTicket),
                    WaitingTickets = _user.Tickets.Where<Ticket>(t => t.TicketStatusId == (int)TicketStatusEnum.WaitingTicket),
                    SoldTickets = _user.Tickets.Where<Ticket>(t => t.TicketStatusId == (int)TicketStatusEnum.SoldTicket),

                };
                return View(_userTickets);
            };
            return RedirectToAction("Events", "Main");
        }


        public IActionResult MyOrders()
        {
            User _user = _repo.GetUserByEmail(HttpContext.User.Identity.Name);
            if (_user != null)
            {
                return View(_user);
            };
            return RedirectToAction("Events", "Main");
        }


        public IActionResult MyAccount()
        { 
            User _user = _repo.GetUserByEmail(HttpContext.User.Identity.Name);
            if (_user != null)
            {
                return View(_user);
            };
            return RedirectToAction("Events", "Main");
        }

    }
}
