﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Reselling.Domain.Infrastructure;
using Reselling.Domain.Models.Data;
using Reselling.UIView.Models.Data;
using System;
using System.Linq;
using System.Threading;

namespace Resaling.Controllers
{
    public class MainController : Controller
    {

        IRepository _repo;
        readonly int _eventsPageSize = 4;
        readonly int _ticketsForEventsPageSize = 4;


        public MainController(IRepository repo)
        {
            _repo = repo;
        }

        // GET: /<controller>/
        public IActionResult Events(int? page)
        {
            int _page = (int)(page ?? 1);

            EventsListViewModel _eventsList = new EventsListViewModel
            {
                EventsItems = _repo.GetEvents().Skip((_page - 1) * _eventsPageSize).Take(_eventsPageSize),
                PagingInfo = new PagingInfo
                {
                    CurentPage = _page,
                    ItemsRerPage = _eventsPageSize,
                    TotalItems = _repo.GetEvents().Count()
                }
            };


            return View(_eventsList);
        }

        public IActionResult TicketsEvent(int? id, int? page)
        {
            EventTicketsListView _eventsList;

            if (id != null)
            {
                int _id = (int)id;

                Event _event = _repo.GetEventById(_id);

                if (_event != null)
                {
                    int _page = (int)(page ?? 1);
                    _eventsList = new EventTicketsListView
                    {
                        Event = _event,


                        PagingInfo = new PagingInfo
                        {
                            CurentPage = _page,
                            ItemsRerPage = _ticketsForEventsPageSize,
                            TotalItems = _event.Tickets.Count()
                        }
                    };
                    return View(_eventsList);
                }
            }
            return RedirectToAction("Events");
        }


        public IActionResult SetLanguage(string culture, string returnUrl)
        {

            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

                     
             return LocalRedirect(returnUrl);

            // return View(System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName);culture
        }
    }
}
