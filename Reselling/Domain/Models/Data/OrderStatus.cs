﻿using System.Collections.Generic;

namespace Reselling.Domain.Models.Data
{
    public class OrderStatus
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<Order> Orders { get; set; }
    }
}
