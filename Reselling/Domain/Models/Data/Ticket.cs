﻿using Reselling.Domain.Models.Authentication;

namespace Reselling.Domain.Models.Data
{
    public class Ticket
    {
        public int Id { get; set; }

        public decimal Price { get; set; }

        public string Note { get; set; }

        public int? EventId { get; set; }

        public Event Event { get; set; }

        public int? UserId { get; set; }

        public User User { get; set; }

        public int? TicketStatusId { get; set; }

        public virtual TicketStatus TicketStatus { get; set; }
    }
}