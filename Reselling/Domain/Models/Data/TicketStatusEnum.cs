﻿namespace Reselling.Domain.Models.Data
{
    public enum TicketStatusEnum
    {
        SellingTicket = 1,
        WaitingTicket,
        SoldTicket
    }
}
