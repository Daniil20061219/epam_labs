﻿using Reselling.Domain.Models.Authentication;

namespace Reselling.Domain.Models.Data
{
    public class Order
    {
        public int Id { get; set; }

        public int? TicketId { get; set; }

        public Ticket Ticket { get; set; }

        public int? UserId { get; set; }

        public User User { get; set; }

        public int? OrderStatusId { get; set; }

        public virtual OrderStatus OrderStatus { get; set; }

        public int TrackNumber { get; set; }


    }
}