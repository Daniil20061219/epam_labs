﻿using System.Collections.Generic;


namespace Reselling.Domain.Models.Data
{
    public class Venue
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Adress { get; set; }

        public int? CityId { get; set; }

        public City City { get; set; }

        public IEnumerable<Event> Events { get; set; }

        
    }
}