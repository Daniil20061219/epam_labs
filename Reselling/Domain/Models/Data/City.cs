﻿using System.Collections.Generic;

namespace Reselling.Domain.Models.Data
{
    public class City
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<Venue> Venues { get; set; }

    }
}