﻿using System.Collections.Generic;
using Reselling.Domain.Models.Data;

namespace Reselling.Domain.Models.Authentication
{
    public class User
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Country { get; set; }

        public string Adress { get; set; }

        public string PhoneNumber { get; set; }

        public string Password { get; set; }

        public string Cookies { get; set; }

        public int? RoleId { get; set; }

        public virtual Role Role { get; set; }

        public IEnumerable<Ticket> Tickets { get; set; }

        public IEnumerable<Order> Orders { get; set; }
    }
}