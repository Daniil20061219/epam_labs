﻿using Reselling.Domain.Models.Authentication;
using Reselling.Domain.Models.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Reselling.Domain.Infrastructure
{
    public interface IRepository
    {
        User GetUserById(int id);

        User GetUserByEmail(string email);

        Task<User> GetUserByEmailAsync(string email);

        Task<User> GetUserByEmailAndPasswordAsync(string email, string password);

        Event GetEventById(int id);

        IEnumerable<Event> GetEvents();

        Ticket GetTicketById(int id);

        IEnumerable<Ticket> TicketsByUser(int id);

        IEnumerable<Ticket> TicketsByEvent(int id);

        IEnumerable<Order> OrdersByUser(int id);

        void AddUser(string email, string password);


    }
}
